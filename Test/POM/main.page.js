var mainPage = function() {

    // Variables ********************************************************************************
    title = element(by.tagName('h3')); // by TagName

    this.getTitle = async function() {
      return title;
    };

    result = element(by.tagName('h2')); // by TagName

    this.getResult = async function() {
      return result;
    };

    input_left = element(by.xpath('/html/body/div/div/form/input[1]')); // by xpath

    this.getInputLeft = async function() {
      return input_left;
    };

    input_right = element(by.xpath('/html/body/div/div/form/input[2]')); // by xpath

    this.getInputRight = async function() {
      return input_right;
    };

    input_operation = element(by.tagName('select')); // by TagName

    this.getInputOperation = async function() {
      return input_operation;
    };

    go_button = element(by.id('gobutton')); // by ID

    this.getGoButton = async function() {
      return go_button;
    };

    history_label = element(by.tagName('h4')); // by TagName

    this.getHistoryLabel = async function() {
      return history_label;
    };

    // Functions ********************************************************************************
    this.getPage = async function() {
      await browser.get("http://juliemr.github.io/protractor-demo/");
      await browser.sleep(1000)
      await browser.manage().window().maximize();
    };

    this.setInputLeft = async function(value) {
      await input_left.sendKeys(value);
    };

    this.setInputRight = async function(value) {
      await input_right.sendKeys(value);
    };

    this.ClickGoButton = async function(){
      go_button.click();
    };
  };
  module.exports = new mainPage();