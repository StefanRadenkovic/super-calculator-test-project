const { getInputLeft, getInputRight } = require('../POM/main.page.js');
var mainPage = require('../POM/main.page.js');

var EC = protractor.ExpectedConditions; //Library of conditions for protractor

describe('Super Calculator\'s Subtraction operator,', function(){
    beforeEach(async ()=>{
        await mainPage.getPage()

        //change operation
        element(by.tagName("select")).click();
        element.all(by.tagName("option")).then(function(items) {
            items[4].click();
        });
    });

    afterEach(function() {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });

    it('Verify that subtraction of two positive integers works.', async function(){
        mainPage.setInputLeft(5);
        mainPage.setInputRight(2);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('3');
    });

    it('Verify that subtraction of positive integer and zero works.', async function(){
        mainPage.setInputLeft(2);
        mainPage.setInputRight(0);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('2');
    });

    it('Verify that subtraction of two negative integers works.', async function(){
        mainPage.setInputLeft(-4);
        mainPage.setInputRight(-9);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('5');
    });

    it('Verify that subtraction of positive and negative integers works.', async function(){
        mainPage.setInputLeft(6);
        mainPage.setInputRight(-9);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('15');
    });

    it('Verify that subtraction of negative integers and zero works.', async function(){
        mainPage.setInputLeft(0);
        mainPage.setInputRight(-15);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('15');
    });

    it('Verify that subtraction of positive integer and decimal works.', async function(){
        mainPage.setInputLeft(5);
        mainPage.setInputRight(12.3);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('-6.7');
    });

    it('Verify that subtraction of positive integer and negative decimal works.', async function(){
        mainPage.setInputLeft(67);
        mainPage.setInputRight(-12.3);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('79.3');
    });

    it('Verify that subtraction of two negative decimal numbers works.', async function(){
        mainPage.setInputLeft(-67);
        mainPage.setInputRight(-12.3);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('-54.7');
    });

    it('Verify that subtraction with empty left operand gives NaN error.', async function(){
        mainPage.setInputRight(5);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that subtraction with empty right operand gives NaN error.', async function(){
        mainPage.setInputLeft(1);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that subtraction of empty operands gives NaN error.', async function(){
        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that subtraction with left text operand gives NaN error.', async function(){
        mainPage.setInputLeft("bad input");
        mainPage.setInputRight(8);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that subtraction with left text operand gives NaN error.', async function(){
        mainPage.setInputLeft(5279);
        mainPage.setInputRight('text');

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that subtraction with both text operands gives NaN error.', async function(){
        mainPage.setInputLeft("bad input");
        mainPage.setInputRight('text');

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });
});