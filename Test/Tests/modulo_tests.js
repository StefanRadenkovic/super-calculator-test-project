const { getInputLeft, getInputRight } = require('../POM/main.page.js');
var mainPage = require('../POM/main.page.js');

var EC = protractor.ExpectedConditions; //Library of conditions for protractor

describe('Super Calculator\'s Modulo operator,', function(){
    beforeEach(async ()=>{
        await mainPage.getPage()

        //change operation
        element(by.tagName("select")).click();
        element.all(by.tagName("option")).then(function(items) {
            items[2].click();
        });
    });

    afterEach(function() {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });

    it('Verify that modulo of two positive integers works.', async function(){
        mainPage.setInputLeft(5);
        mainPage.setInputRight(2);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('1');
    });

    it('Verify that modulo of positive integer and zero works.', async function(){
        mainPage.setInputLeft(2);
        mainPage.setInputRight(0);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that modulo of positive integer and one works.', async function(){
        mainPage.setInputLeft(365);
        mainPage.setInputRight(1);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('0');
    });

    it('Verify that modulo of positive integer and itself works.', async function(){
        mainPage.setInputLeft(27);
        mainPage.setInputRight(27);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('0');
    });

    it('Verify that modulo of smaller left integer and larger right integer works.', async function(){
        mainPage.setInputLeft(5);
        mainPage.setInputRight(27);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('5');
    });

    it('Verify that modulo of two negative integers works.', async function(){
        mainPage.setInputLeft(-4);
        mainPage.setInputRight(-9);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('-4');
    });

    it('Verify that modulo of positive and negative integers works.', async function(){
        mainPage.setInputLeft(6);
        mainPage.setInputRight(-9);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('6');
    });

    it('Verify that modulo of negative integers and zero works.', async function(){
        mainPage.setInputLeft(0);
        mainPage.setInputRight(-15);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('0');
    });

    it('Verify that modulo of positive integer and decimal works.', async function(){
        mainPage.setInputLeft(5);
        mainPage.setInputRight(12.3);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('5');
    });

    it('Verify that modulo of positive integer and negative decimal works.', async function(){
        mainPage.setInputLeft(67);
        mainPage.setInputRight(-12.3);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('-5.5');
    });

    it('Verify that modulo of two negative decimal numbers works.', async function(){
        mainPage.setInputLeft(-67);
        mainPage.setInputRight(-12.3);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('5.5');
    });

    it('Verify that modulo with empty left operand gives NaN error.', async function(){
        mainPage.setInputRight(5);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that modulo with empty right operand gives NaN error.', async function(){
        mainPage.setInputLeft(1);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that modulo of empty operands gives NaN error.', async function(){
        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that modulo with left text operand gives NaN error.', async function(){
        mainPage.setInputLeft("bad input");
        mainPage.setInputRight(8);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that modulo with left text operand gives NaN error.', async function(){
        mainPage.setInputLeft(5279);
        mainPage.setInputRight('text');

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that modulo with both text operands gives NaN error.', async function(){
        mainPage.setInputLeft("bad input");
        mainPage.setInputRight('text');

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });
});