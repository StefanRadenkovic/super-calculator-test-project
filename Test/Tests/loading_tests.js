const { getInputLeft, getInputRight } = require('../POM/main.page.js');
var mainPage = require('../POM/main.page.js');

var EC = protractor.ExpectedConditions; //Library of conditions for protractor

describe('Super Calculator\'s Page loads,', function(){

    beforeEach(async ()=>{
        await mainPage.getPage()
    })

    afterEach(function() {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });

    it('Verify that the Title label loads and is spelled correctly.', async function(){

        var title = await mainPage.getTitle();

        expect(title.isPresent()).toBe(true);
        expect(title.isEnabled()).toBe(true);
        expect(title.isDisplayed()).toBe(true);
        expect(title.getText()).toBe("Super Calculator");
    });

    it('Verify that the left input field loads and is usable.', async function(){

        var left = await mainPage.getInputLeft();

        expect(left.isPresent()).toBe(true);
        expect(left.isEnabled()).toBe(true);
        expect(left.isDisplayed()).toBe(true);
    });

    it('Verify that the right input field loads and is usable.', async function(){

        var right = await mainPage.getInputRight();

        expect(right.isPresent()).toBe(true);
        expect(right.isEnabled()).toBe(true);
        expect(right.isDisplayed()).toBe(true);
    });

    it('Verify that the operation input field loads and is usable.', async function(){

        var operation = await mainPage.getInputOperation();

        expect(operation.isPresent()).toBe(true);
        expect(operation.isEnabled()).toBe(true);
        expect(operation.isDisplayed()).toBe(true);
    });

    it('Verify that the go button loads and is usable.', async function(){

        var button = await mainPage.getGoButton();

        expect(button.isPresent()).toBe(true);
        expect(button.isEnabled()).toBe(true);
        expect(button.isDisplayed()).toBe(true);
    });

    it('Verify that the result label loads and is visible.', async function(){

        var result = await mainPage.getResult();

        expect(result.isPresent()).toBe(true);
        expect(result.isEnabled()).toBe(true);
        expect(result.isDisplayed()).toBe(true);
    });

    it('Verify that the history label loads and is visible.', async function(){

        var history = await mainPage.getHistoryLabel();

        expect(history.isPresent()).toBe(true);
        expect(history.isEnabled()).toBe(true);
        expect(history.isDisplayed()).toBe(true);
    });

    it('Verify that the table header Time label loads and is visible.', async function(){

        element.all(by.tagName("th")).then(function(label){
            expect(label[0].isPresent()).toBe(true);
            expect(label[0].isEnabled()).toBe(true);
            expect(label[0].isDisplayed()).toBe(true);
        });
    });

    it('Verify that the table header Expression label loads and is visible.', async function(){

        element.all(by.tagName("th")).then(function(label){
            expect(label[1].isPresent()).toBe(true);
            expect(label[1].isEnabled()).toBe(true);
            expect(label[1].isDisplayed()).toBe(true);
        });
    });

    it('Verify that the table header Result label loads and is visible.', async function(){

        element.all(by.tagName("th")).then(function(label){
            expect(label[2].isPresent()).toBe(true);
            expect(label[2].isEnabled()).toBe(true);
            expect(label[2].isDisplayed()).toBe(true);
        });
    });
});