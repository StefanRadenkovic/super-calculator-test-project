const { getInputLeft, getInputRight } = require('../POM/main.page.js');
var mainPage = require('../POM/main.page.js');

var EC = protractor.ExpectedConditions; //Library of conditions for protractor

describe('Super Calculator\'s Division operator,', function(){
    beforeEach(async ()=>{
        await mainPage.getPage()

        //change operation
        element(by.tagName("select")).click();
        element.all(by.tagName("option")).then(function(items) {
            items[1].click();
        });
    });

    afterEach(function() {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });

    it('Verify that division of two positive integers works.', async function(){
        mainPage.setInputLeft(5);
        mainPage.setInputRight(2);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('2.5');
    });

    it('Verify that division of positive integer and zero works.', async function(){
        mainPage.setInputLeft(2);
        mainPage.setInputRight(0);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('Infinity');
    });

    it('Verify that division of two negative integers works.', async function(){
        mainPage.setInputLeft(-4);
        mainPage.setInputRight(-9);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('0.4444444444444444');
    });

    it('Verify that division of positive and negative integers works.', async function(){
        mainPage.setInputLeft(6);
        mainPage.setInputRight(-3);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('-2');
    });

    it('Verify that division of negative integers and zero works.', async function(){
        mainPage.setInputLeft(0);
        mainPage.setInputRight(-15);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('0');
    });

    it('Verify that division of positive integer and decimal works.', async function(){
        mainPage.setInputLeft(25);
        mainPage.setInputRight(2.5);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('10');
    });

    it('Verify that division of positive integer and negative decimal works.', async function(){
        mainPage.setInputLeft(25);
        mainPage.setInputRight(-2.5);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('-10');
    });

    it('Verify that division of two negative decimal numbers works.', async function(){
        mainPage.setInputLeft(-6.4);
        mainPage.setInputRight(-2.2);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('2.9090909090909090');
    });

    it('Verify that division with empty left operand gives NaN error.', async function(){
        mainPage.setInputRight(5);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that division with empty right operand gives NaN error.', async function(){
        mainPage.setInputLeft(1);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that division of empty operands gives NaN error.', async function(){
        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that division with left text operand gives NaN error.', async function(){
        mainPage.setInputLeft("bad input");
        mainPage.setInputRight(8);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that division with left text operand gives NaN error.', async function(){
        mainPage.setInputLeft(5279);
        mainPage.setInputRight('text');

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });

    it('Verify that division with both text operands gives NaN error.', async function(){
        mainPage.setInputLeft("bad input");
        mainPage.setInputRight('text');

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        expect(element(by.tagName('h2')).getText()).toBe('NaN');
    });
});