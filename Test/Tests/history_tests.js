const { getInputLeft, getInputRight } = require('../POM/main.page.js');
var mainPage = require('../POM/main.page.js');

var EC = protractor.ExpectedConditions; //Library of conditions for protractor

describe('Super Calculator\'s history saving,', function(){
    beforeEach(async ()=>{
        await mainPage.getPage()

        //change operation
        element(by.tagName("select")).click();
        element.all(by.tagName("option")).then(function(items) {
            items[3].click();
        });
    });

    afterEach(function() {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });

    it('Verify that history saving works.', async function(){
        mainPage.setInputLeft(5);
        mainPage.setInputRight(-27);

        mainPage.ClickGoButton();
        browser.sleep(2000); // wait for operation to calculate

        element.all(by.tagName("td")).then(function(record_items){
            expect(record_items[1].getText()).toBe("5 * -27");
            expect(record_items[2].getText()).toBe("-135");
        });
    });
});