var consoleReporter = require(`jasmine-custom-reporters/spec-console-reporter`);
const dir = "Test/Tests/";
exports.config = {
    framework: 'jasmine2',
    capabilities: {
    browserName: 'chrome',
    },
    specs: [dir+'history_tests.js'],
    //specs: [dir+'loading_tests.js', dir+'addition_tests.js', dir+'subtraction_tests.js', dir+'multiplication_tests.js', dir+'division_tests.js', dir+'modulo_tests.js', dir+'history_tests.js'],
    onPrepare: async function() {
        var jasmineReporters = require('jasmine-reporters');
        await browser.waitForAngularEnabled(false);
        jasmine.getEnv().addReporter(consoleReporter);
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: 'testresults',
            filePrefix: 'xmloutput'
        }));
    }
};