describe('Google\'s Search Functionality',  function() {
    it('can find search results', async function() {
    
    await browser.driver.get('https://google.com/');
    await element(by.name('q')).sendKeys('BrowserStack');
    await browser.sleep(1000)
    await element(by.name('btnK')).click();
    
    //title of the launched webpage is expected to be BrowserStack - Google Search
    await browser.sleep(1000)
    var title = await browser.getTitle();
    expect(title).toEqual('BrowserStack - Google претрага');
    });
});